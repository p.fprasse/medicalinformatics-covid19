# medicalinformatics-covid19

Sammlung von Infos, Tools und Aktivitäten für medizininformatische Unterstützung der Covid-19-Krise   

# COVID-19 Tools und Medizininformatischer Support

## Existierende Tools


| Was  | Wo | Wer | Woher |
|------|----|-----|-------|
| App zur Unterstützung der Dokumentation bei Corona | apk: https://drive.google.com/open?id=17dE2tBrMZa97x7C4k8nstoM9q1LajmrE|Medizininformatik Münster | MD |
|IOS-App zur Dokumentation des Krankheitsverlaufs|https://devpost.com/software/corona-app-feldtest|Kairos|ST|
|openEHR-Meldeunterstützung|https://github.com/AppertaFoundation/COVID-19-screening-interface/tree/develop| Apperta Foundation| BH| 
|  Ressourcenplanung für Kliniken | http://predictivehealthcare.pennmedicine.org/2020/03/14/accouncing-chime.html  | Penn Medicine    |   twitter  "covid informatics"   |
| Fragebogen für die Einschätzung eines Verdachts |https://covapp.charite.de/| Charité| ST |


## Terminologien und Standards

| Was  | Wo | Wer | Woher |
|------|----|-----|-------|
| ICD-Codes: ICD-10: U07.1 ICD-11: RA01.0 |  https://www.who.int/classifications/icd/covid19/en/ | WHO | twitter  "covid informatics" |
| SNOMED CT Extension | http://www.snomed.org/news-and-events/articles/march-2020-interim-snomedct-release-COVID-19 | SNOMED international | twitter  "covid informatics" | 
| US CDC - Data dictionary and report forms | https://www.cdc.gov/coronavirus/2019-ncov/php/reporting-pui.html| US-Behörde| Internetrecherche |
|WHO Case Report Forms |https://apps.who.int/iris/bitstream/handle/10665/331234/WHO-2019-nCoV-SurveillanceCRF-2020.2-eng.pdf| WHO| Internetreche|
|WHO Data dictionary |https://www.who.int/docs/default-source/coronaviruse/data-dictionary-covid-crf-v6.xlsx?sfvrsn=a7d4ef98_2|WHO| Internetrecherche|
|Kontaktpersonenliste |https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Kontaktperson/Kontaktpersonenliste.xlsx?__blob=publicationFile| RKI| covid-support-chat |
|Kurzfragebogen für Kontaktpersonen|https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Kontaktperson/Tagebuch_Kontaktpersonen.docx?__blob=publicationFile|[RKI](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Kontaktperson/Dokumente_Tab.html)| covid-support-chat|
|openEHR Templates |https://openehr.org/ckm/incubators/1013.30.80|[openEHR](https://www.openehr.org/news_events/openehr_news/311) |openEHR ||
|Confirmed Covid-19 infection report  | https://openehr.org/ckm/templates/1013.26.271 | Epidemiologischer Bericht bei Bestätigung einer COVID-19-Infektion ||
| Suspected COVID-19 risk assessment v0 | https://openehr.org/ckm/templates/1013.26.267 | Risikobewertung einer Covid-19-Infektion aus Screening oder Selbstbewertung (NHS in Großbritannien) ||
| Suspected COVID-19 risk assessment v0.1 | https://openehr.org/ckm/templates/1013.26.280 | Siehe 2. Nur neuere Version! ||
| AU COVID-19 Likelihood Assessment | https://openehr.org/ckm/templates/1013.26.273 | Screening-Bewertungsdatensatz für COVID-19, der vom Northern Territory, Australien verwendet wird||


## Hilfreiche Infos allgemeinerer Art

| Was  | Wo | Wer | Woher |
|------|----|-----|-------|
| FHIR-Basisprofile Deutsch | https://simplifier.net/guide/basisprofil-de-r4/home | HL7-D | ST |
| Implementierungsleitfaden Meldepflichtige Krankheiten |http://wiki.hl7.de/index.php?title=IG:%C3%9Cbermittlung_meldepflichtiger_Krankheiten_Labormeldung | HL7 | ST |


## Zusammenstellungen

| Was  | Wo | Anmerkung | Woher |
|------|----|-----|-------|
|  Kuratierte Liste von github-Repos und weiterem | https://github.com/soroushchehresa/awesome-coronavirus  | Das meiste sind Visualisierungen der öffentlichen Daten |   github  |
| Nützliche Tools für die Corona-Krise | https://wirvsvirushackathon.org/ressourcen/ | vor allem für den privat/öffentlichen Bereich,z.B. Nachbarschaftshilfen | Internetrecherche |


### Initiativen

| Was  | Wo | Wer | Woher | Infos |
|------|----|-----|-------|-------|
| Register für Covid-Erkrankungen | https://leoss.net/ | DGI, DGZI | ST, Ärzteblatt |
| IT-Lösungen für Corona | https://wirvsvirushackathon.org/ | Bundesregierung | medinfo-chat | 20.3.-23.3. |
|MSDA - harmonized data collection on for Covid-19 affecting MS. | % |email| weiter Abstimmung in den nächsten Tagen |
|Covid-19 modeling portal |https://midasnetwork.us/covid-19/| MIDAS Coordination Center University of Pittsburgh | github "covid 19"| Data & Software Ressources |  
|
